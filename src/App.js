import React, { Component } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import NavBar from "./components/navbar";
import Products from "./components/products";
import Posts from "./components/posts";
import Home from "./components/home";
import Dashboard from "./components/admin/dashboard";
import ProductDetails from "./components/productDetails";
import NotFound from "./components/notFound";
import "./App.css";


class App extends Component {

  render() {
    return (
      <div>
        <NavBar />
        <Routes>
            <Route path="/products/:id" element={<ProductDetails/>} />
            <Route path="/products" element={<Products />}  />
            <Route index path="/posts" element={<Posts blogPost="post"/>} />
            <Route path="/posts/:year/:month" element={<Posts blogPost="post"/>} />
            <Route path="/admin" element={<Dashboard />} />
            <Route path="/" element={<Home />} />
            <Route path="/not-found" element={<NotFound />} />
            <Route path="*" element={<Navigate to={"/not-found"} replace />}/>
          
        </Routes>
      </div>
    );
  }
}

export default App;
