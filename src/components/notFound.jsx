import React from "react";
import { useNavigate } from "react-router-dom";

const NotFound = () => {
  const navigate = useNavigate();
  return <h1>Not Found
     <button onClick={() => navigate(-1)}>back</button>
  </h1>;
};

export default NotFound;
